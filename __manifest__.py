# -*- coding: utf-8 -*-
{
    'name': "Employee Car Request",

    'summary': """
        Request a car and get approval""",

    'description': """
        Long description of module's purpose
        Long description of module's purpose
        Long description of module's purpose
    """,

    'author': "Abdul Hameed Sattar",
    'website': "http://www.nauss.edu.sa",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Human Resource',
    'version': '0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','fleet',],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
