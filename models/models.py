# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class CarRequest(models.Model):
    _name = 'car.request' # Table in DB => car_request
    _inherit = ['mail.thread']
    _description = 'Car Request'

    name = fields.Char(string="Request", required=True, )
    date_from = fields.Datetime(string="Starting Date", default=fields.Datetime.now(), )
    date_to = fields.Datetime(string="End Date", required=False, )
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Employee", required=True, )
    car_id = fields.Many2one(comodel_name="fleet.vehicle", string="Car", required=True, )
    state = fields.Selection(string="Status", selection=[('draft', 'Draft'), ('confirm', 'Confirm'),
                                                         ('validate', 'Validate'), ('refuse', 'Refuse'),
                                                         ('approved', 'Approved'), ],
                             default="draft", track_visibility='onchange', )
    email = fields.Char(string="Email", required=False, )
    seq_name = fields.Char(string="Order Reference", readonly=True, required=True, copy=False,
                           index=True, default=lambda self: _('New'))
    website = fields.Char(string="Website", required=False, )

    # _sql_constraints is working on DB level and you can find it it the table:
    # \d TABLE NAME
    # SQL Constraints can use to rules: unique & check
    _sql_constraints = [
        ('unique_email', 'unique(email)', 'ERROR: The email should be unique.'),
    ]

    @api.constrains('email')
    def _check_email(self):
        """
        Constrains will check / triggered for the listed fields only in creation and updating.
        :return:
        """
        if self.email.endswith('gmail.com'):
            raise ValidationError("Gmail is not accepted!")
        if self.email.endswith('yahoo.com'):
            raise ValidationError("Yahoo is not accepted!")

    def confirm_request(self):
        self.state = 'confirm'

    def validate_request(self):
        self.state = 'validate'

    def refuse_request(self):
        self.state = 'refuse'

    def approve_request(self):
        self.state = 'approved'

    @api.model
    def create(self, vals):
        if vals.get('seq_name', _('New')) == _('New'):
            vals['seq_name'] = self.env['ir.sequence'].next_by_code('employee.car.request.sequence') or _('New')
        result = super(CarRequest, self).create(vals)
        return result

    @api.onchange('email')
    def _onchange_email(self):
        """
        Email: YOURNAME@COMPANY.COM
        Website: http://WWW.COMPANY.COM
        :return:
        """
        result = {}
        if self.email:
            # self.website = 'https://www.%s' % (self.email.split('@')[1])
            result.update({
                'value': {
                    'website': 'https://www.%s' % (self.email.split('@')[1])
                },
                'warning': {
                    'title': 'Congrates!',
                    'message': 'You have added an email id.'
                },
                'domain': {
                    'employee_id': [('id', '!=', '20')]
                }
            })
            return result

